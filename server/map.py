import time
import os
import sys
import datetime
import threading
import logging
import traceback
import json
import re
import string
import random
import numpy
from actions import ActionMethods
from sys import platform
from random import randint
if platform == "linux" or platform == "linux2":
	import prctl
from pathlib import Path
from config import Cfg, Enviroment

class MapThread(threading.Thread):
	ps = None
	forceStop = False
	x,y = (15, 15)
	matrix = None
	player_pos = None
	player_stats = [100, 100, 100, 100, 100]
	message_state = [0, 0, 0, 0, 0]

	def __init__(self, ps):
		super(MapThread, self).__init__(name="MapThread")
		logging.info("Map Thread launching")
		self.matrix = [[0 for zx in range(self.x)] for zy in range(self.y)]
		self.ps = ps
		self.ps.map = self

	def run(self):
		try:
			prctl.set_name("MapThread")
		except NameError:
			pass
		self.mapStart()

	def mapStart(self):
		try:
			logging.info("Map Thread starting")
			self._preFillStations()
			pos = self.getRandomPosition()
			self.player_pos = [pos[0], pos[1]]
			self._debugPrintMatrix()	
			self.think_forever()
		except:
			if Cfg.ENV.value <= Enviroment.Beta.value:
				logging.warning("Map Thread interrupted. Relaunching…")
				self.mapStart()
			else:
				logging.error("Map Thread interrupted. Not relaunching in dev env.")
				traceback.print_exc()

	def think_forever(self):
		isRunning = False
		while self.forceStop != True:
			if isRunning == False:
				logging.info("Map Thread started")
				isRunning = True

			if random.randint(0, 3) == 1 and self.player_stats[0] > 0:
				self.player_stats[0] -= 1

			if random.randint(0, 6) == 1 and self.player_stats[1] > 0:
				self.player_stats[1] -= 1

			if random.randint(0, 4) == 1 and self.player_stats[2] > 0:
				self.player_stats[2] -= 1

			if random.randint(0, 2) == 1 and self.player_stats[3] > 0:
				self.player_stats[3] -= 1

			if random.randint(0, 8) == 1 and self.player_stats[4] > 0:
				self.player_stats[4] -= 1

			time.sleep(0.5)

	def _preFillStations(self):
		# hunger
		for i in range(5):
			pos = self.getRandomPosition()
			self.matrix[pos[0]][pos[1]] = "A"

		# hygiene
		for i in range(2):
			pos = self.getRandomPosition()
			self.matrix[pos[0]][pos[1]] = "B"

		# energy
		for i in range(6):
			pos = self.getRandomPosition()
			self.matrix[pos[0]][pos[1]] = "C"

		# entertainment
		for i in range(10):
			pos = self.getRandomPosition()
			self.matrix[pos[0]][pos[1]] = "D"

		# social
		for i in range(2):
			pos = self.getRandomPosition()
			self.matrix[pos[0]][pos[1]] = "E"

	def getRandomPosition(self, call=1):
		if call > self.x*self.y:
			return (0, 0)

		rand_x = randint(0, self.x-1)
		rand_y = randint(0, self.y-1)
		if self.matrix[rand_x][rand_y] == 0:
			return (rand_x, rand_y)
		else:
			return self.getRandomPosition(call+1)

	def checkCollision(self, axis, secondval, start, end):
		minim = min(start, end)
		maxim = max(start, end)
		if axis == "y":
			for u in range(minim, maxim+1):
				if self.matrix[u][secondval] != 0:
					return True
		elif axis == "x":
			for u in range(minim, maxim+1):
				if self.matrix[secondval][u] != 0:
					return True
		return False

	def isNearObjective(self, pos, obj):
		if pos[1] == 0:
			y_range = range(0, 1+1)
		elif pos[1] == (self.y-1):
			y_range = range(self.y-2, self.y-1+1)
		else:
			y_range = range(pos[1]-1, pos[1]+1+1)

		if pos[0] == 0:
			x_range = range(0, 1+1)
		elif pos[0] == (self.x-1):
			x_range = range(self.x-2, self.x-1+1)
		else:
			x_range = range(pos[0]-1, pos[0]+1+1)


		for y in y_range:
			for x in x_range:
				if self.matrix[x][y] == obj:
					return True

		return False

	def getMessages(self):
		messages = []
		codes = ["A", "B", "C", "D", "E"]
		for i in range(0, 5):
			if self.player_stats[i] > 75:
				self.message_state[i] = 0
			elif self.player_stats[i] > 30 and self.message_state[i] == 0:
				self.message_state[i] = 1
				messages.append({"code": 200, "action": 2, "stat": codes[i], "level": self.player_stats[i]})
			elif self.player_stats[i] <= 30 and self.message_state[i] == 1:
				self.message_state[i] = 2
				messages.append({"code": 200, "action": 2, "stat": codes[i], "level": self.player_stats[i]})

		return messages

	def _debugPrintMatrix(self):
		for row in self.matrix:
			for col in row:
				print(col, end=" ")
			print("")
