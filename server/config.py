from enum import Enum


class Enviroment(Enum):
	Live = -1
	Beta = 0
	Debug = 5

class Cfg():

	# Global
	ENV = Enviroment.Debug
	THREADS = ["BrainThread", "HTTPServerThread", "MapThread"]
	# /Global
