import json
import re
from urllib.request import urlopen
import json
from collections import OrderedDict

'''
	Action codes returned by the methods:
		- 1 - walk
		- 2 - stats
		- 3 - basic
		- 4 - cat
		- 5 - eat
		- 6 - hygiene
		- 7 - energy
		- 8 - entertainment
		- 9 - social
'''

class ActionMethods():
	ps = None
	brain = None

	def __init__(self, ps, bt):
		self.ps = ps
		self.brain = bt

	def __inMessage(self, k, m):
		for keyword in m.split():
			if k == keyword:
				return True
		return False

	def action_test(self, k, m): 
		return {"code": 200}

	def action_walk(self, data, m): 
		direction = "none"
		units_count = int(data["count"])
		if units_count < 1:
			return {"code": 602}

		if data["direction"] in ("left", "west"):
			direction = "left"

			if self.ps.map.player_pos[1]-units_count < 0:
				return {"code": 601}
			if self.ps.map.checkCollision("x", self.ps.map.player_pos[0], self.ps.map.player_pos[1], self.ps.map.player_pos[1]-units_count):
				return {"code": 603}

			self.ps.map.player_pos[1] -= units_count
		elif data["direction"] in ("top", "up", "north"):
			direction = "top"

			if self.ps.map.player_pos[0]-units_count < 0:
				return {"code": 601}
			if self.ps.map.checkCollision("y", self.ps.map.player_pos[1], self.ps.map.player_pos[0], self.ps.map.player_pos[0]-units_count):
				return {"code": 603}

			self.ps.map.player_pos[0] -= units_count
		elif data["direction"] in ("bottom", "down", "south"):
			direction = "bottom"

			if self.ps.map.player_pos[0]+units_count >= self.ps.map.y:
				return {"code": 601}
			if self.ps.map.checkCollision("y", self.ps.map.player_pos[1], self.ps.map.player_pos[0], self.ps.map.player_pos[0]+units_count):
				return {"code": 603}

			self.ps.map.player_pos[0] += units_count
		elif data["direction"] in ("right", "east"):
			direction = "right"

			if self.ps.map.player_pos[1]+units_count >= self.ps.map.x:
				return {"code": 601}
			if self.ps.map.checkCollision("x", self.ps.map.player_pos[0], self.ps.map.player_pos[1], self.ps.map.player_pos[1]+units_count):
				return {"code": 603}

			self.ps.map.player_pos[1] += units_count

		return {"code": 200, "action": 1, "direction": direction, "count": units_count}

	def action_basic(self, r): 
		return {"code": 200, "action": 3, "response": r}

	def action_cat(self, k, m):
		try:
			response = urlopen("http://catfacts-api.appspot.com/api/facts")
			data = json.loads(response.read())
		except: 
			print(response.msg)
			print(response.status)
			return {"code": 710}

		return {"code": 200, "action": 4, "fact": data["facts"][0]}

	def action_eat(self, k, m):
		if not self.ps.map.isNearObjective(self.ps.map.player_pos, "A"):
			return {"code": 604}

		self.ps.map.player_stats[0] = 100

		return {"code": 200, "action": 5}

	def action_bath(self, k, m):
		if not self.ps.map.isNearObjective(self.ps.map.player_pos, "B"):
			return {"code": 604}

		if self.ps.map.player_stats[0] == 0:
			return {"code": 626}
		elif self.ps.map.player_stats[2] == 0:
			return {"code": 627}

		self.ps.map.player_stats[1] = 100

		return {"code": 200, "action": 6}

	def action_sleep(self, k, m):
		if not self.ps.map.isNearObjective(self.ps.map.player_pos, "C"):
			return {"code": 604}

		if self.ps.map.player_stats[0] == 0:
			return {"code": 625}

		self.ps.map.player_stats[2] = 100

		return {"code": 200, "action": 7}

	def action_play(self, k, m):
		if not self.ps.map.isNearObjective(self.ps.map.player_pos, "D"):
			return {"code": 604}

		if self.ps.map.player_stats[0] == 0:
			return {"code": 623}
		elif self.ps.map.player_stats[2] == 0:
			return {"code": 624}

		self.ps.map.player_stats[3] = 100

		return {"code": 200, "action": 8}

	def action_social(self, k, m):
		if not self.ps.map.isNearObjective(self.ps.map.player_pos, "E"):
			return {"code": 604}

		if self.ps.map.player_stats[0] == 0:
			return {"code": 620}
		elif self.ps.map.player_stats[2] == 0:
			return {"code": 621}
		elif self.ps.map.player_stats[3] == 0:
			return {"code": 622}

		self.ps.map.player_stats[4] = 100

		return {"code": 200, "action": 9}

	def action_level(self, data, m): 
		if data["stat"] == "hunger":
			return {"code": 200, "action": 2, "stat": "A", "level": self.ps.map.player_stats[0]}
		elif data["stat"] == "hygiene":
			return {"code": 200, "action": 2, "stat": "B", "level": self.ps.map.player_stats[1]}
		elif data["stat"] == "energy":
			return {"code": 200, "action": 2, "stat": "C", "level": self.ps.map.player_stats[2]}
		elif data["stat"] == "entertainment":
			return {"code": 200, "action": 2, "stat": "D", "level": self.ps.map.player_stats[3]}
		elif data["stat"] == "social":
			return {"code": 200, "action": 2, "stat": "E", "level": self.ps.map.player_stats[4]}
		
		return {"code": 702}