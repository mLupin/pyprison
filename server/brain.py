import time
import os
import sys
import datetime
import threading
import logging
import traceback
import json
import re
import string
from collections import OrderedDict
from actions import ActionMethods
from sys import platform
if platform == "linux" or platform == "linux2":
	import prctl
from pathlib import Path
from config import Cfg, Enviroment

class BrainThread(threading.Thread):
	ps = None
	forceStop = False
	am = None

	def __init__(self, ps):
		super(BrainThread, self).__init__(name="BrainThread")
		logging.info("Brain Thread launching")
		self.ps = ps
		self.am = ActionMethods(self.ps, self)

	def run(self):
		try:
			prctl.set_name("BrainThread")
		except NameError:
			pass
		self.brainStart()

	def brainStart(self):
		try:
			logging.info("Brain Thread starting")
			self.think_forever()
		except:
			if Cfg.ENV.value <= Enviroment.Beta.value:
				logging.warning("Brain Thread interrupted. Relaunching…")
				self.vrStart()
			else:
				logging.error("Brain Thread interrupted. Not relaunching in dev env.")
				traceback.print_exc()

	def think_forever(self):
		isRunning = False
		while self.forceStop != True:
			if isRunning == False:
				logging.info("Brain Thread started")
				isRunning = True

			time.sleep(3)

	def handleIncomingMessage(self, message):
		original_message = message
		message = self._preHandleMessage(message)
		return self._handleMessage(message)

	def _preHandleMessage(self, message):
		message = message.lower().strip()
		message = ''.join(e for e in message if (e.isalnum() or e == " "))
		with open("keywords/strip.txt", "r") as f:
			for line in f.readlines():
				message = message.replace(line.strip(), "")

		message = message.strip()

		return message

	def _handleKeywords(self, k, m): # self, keywords, message
		#print("in")
		slots = []
		message_words = m
		if len(k) > len(message_words):
			return -1
		i = 0
		for word in k:
			first_word_key = 0
			for key,val in enumerate(message_words):
				first_word_key = key
				message_word = val
				break
			message = " ".join(message_words)
			param_slot = re.match("\[(.*?)\=(.*)\]", word)
			param_fill = re.match("\[(.*?)\]", word)
			if param_slot:
				# type = slot
				pattern = re.match("\[(.*?)\=(.*)\]", word)
				if pattern == None or len(pattern.groups()) < 2:
					return -2
				slot_name = pattern.group(1)
				slot_pattern = pattern.group(2)
				word_slot = re.search(slot_pattern, message)
				if word_slot == None or len(word_slot.groups()) < 1:
					return -2
				slot_value = word_slot.group(len(word_slot.groups()))
				slots.append((slot_name, slot_value))
				message = message[word_slot.span()[1]:].strip()
				message_words = message.split()
			elif param_fill:
				# type = fill
				word_slot = re.search(word[1:-1], message)
				if word_slot == None:
					return -2
				message = message[word_slot.span()[1]:].strip()
				message_words = message.split()
			else:
				if " ".join(k) in message:
					break
				elif message_word != word:
					del message_words[first_word_key]
					return self._handleKeywords(k, message_words)


			i += 1

		print(slots)
		return OrderedDict(slots)

	def _handleMessage(self, message):
		ret = 702
		with open("keywords/actions.txt", "r") as f:
			for line in f.readlines():
				keywords = []
				method = "err"
				try:
					split_line = line.strip().lower().split(" -> ")
					if len(split_line) != 2:
						continue
					key_phrase = split_line[0].strip()
					method = "action_%s" % split_line[1].strip()

					if split_line[1].strip().startswith("response_"):
						method = "action_basic"
				except:
					pass

				search = self._handleKeywords(key_phrase.split(), message.split())
				if search != -1 and search != -2:
					if hasattr(ActionMethods, method) and callable(getattr(ActionMethods, method)):
						calling_method = getattr(ActionMethods, method)
						logging.info("Calling %s" % method)
						if method != "action_basic":
							return calling_method(self.am, search, message)
						else:
							return calling_method(self.am, split_line[1].strip().split("_")[1])
					else:
						return {"code": 703}
				elif search == -1:
					ret = 708
				elif search == -2:
					ret = 709

		return {"code": ret}
