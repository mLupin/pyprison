import time
import os
import sys
from sys import platform
import datetime
import threading
import cgi
import logging
import json
import traceback
import colorsys
import webbrowser
import magic
from pathlib import Path
from collections import OrderedDict
if platform == "linux" or platform == "linux2":
	import prctl
from urllib.parse import urlparse
from http.server import BaseHTTPRequestHandler, HTTPServer
from config import Cfg, Enviroment

http_ps = None


'''
   Reponse codes used in the application:
   	  - 200 - OK
   	  - 404 - unknown HTTP path
   	  - 601 - unable to walk
   	  - 602 - moving in place
   	  - 603 - collision detected
   	  - 604 - too far from an objective
   	  - 620 - cannot social (hunger)
   	  - 621 - cannot social (energy)
   	  - 622 - cannot social (entertainment)
   	  - 623 - cannot entertainment (hunger)
   	  - 624 - cannot entertainment (energy)
   	  - 625 - cannot energy (hunger)
   	  - 626 - cannot hygiene (hunger)
   	  - 627 - cannot hygiene (energy)
      - 701 - received empty message
      - 702 - unknown command
      - 703 - unknown method used in the actions file
      - 707 - unknown error occured
      - 708 - too few arguments (not all slots filled)
      - 709 - at least one slot invalid
      - 710 - connection error
'''

class HTTPServer_RequestHandler(BaseHTTPRequestHandler):
	def log_message(self, format, *args):
		return

	def do_GET(self):
		global http_ps
		content = ""
		parsed_path = urlparse(self.path)
		try:
			params = dict([p.split('=') for p in parsed_path[4].split('&')])
		except:
			params = {}

		if self.path.startswith("/test"):
				content = json.dumps({"code": 200})
				self.send_response(200)
		elif self.path.startswith("/state"):
				content = json.dumps({
					"code": 200, 
					"player_pos": http_ps.map.player_pos, 
					"player_stats": OrderedDict([
							("A", http_ps.map.player_stats[0]),
							("B", http_ps.map.player_stats[1]),
							("C", http_ps.map.player_stats[2]),
							("D", http_ps.map.player_stats[3]),
							("E", http_ps.map.player_stats[4])
						]),
					"matrix": http_ps.map.matrix,
					"messages": http_ps.map.getMessages()
				})
				self.send_response(200)
		else:
			base_url_path = self.path[1:]
			if base_url_path == "":
				base_url_path = "index.html"
			url_path = base_url_path.split("?")[0] 
			file_path = "../front/%s" % url_path 

			if self.path.startswith("/responses"):
				file_path = "keywords/responses.txt" 

			if Path(file_path).is_file():
				with open(file_path, "rb") as f:
					content = f.read()
					self.send_response(200)
					filename, file_extension = os.path.splitext(file_path)
					mime = magic.Magic(mime=True)
					mime_type = mime.from_file(file_path)
					if file_extension == ".css":
						mime_type = "text/css"
					self.send_header('Content-Type', mime_type)
					self.end_headers()
					self.wfile.write(content)
					logging.debug("Requested file %s of mime type %s" % (url_path, mime_type))
					return
			else:
				content = json.dumps({"code": 404})
				self.send_response(404)
				logging.debug("Requested a non-existent file %s (404)" % url_path)


		self.send_header('Content-Type','application/json')
		self.end_headers()
		self.wfile.write(bytes(content, "utf-8"))

	def do_POST(self):
		global http_ps
		content = ""
		brain = http_ps.brain
		parsed_path = urlparse(self.path)
		try:
			params = dict([p.split('=') for p in parsed_path[4].split('&')])
		except:
			params = {}
		#logging.debug(self.headers)

		form = cgi.FieldStorage(
			fp=self.rfile, 
			headers=self.headers,
					environ={'REQUEST_METHOD':'POST',
						'CONTENT_TYPE':self.headers['Content-Type'],
		})

		if self.path.startswith("/input"):
			content = json.dumps({"code": 707, "message": "No valid method found to handle the request"})
			if not "message" in form:
				content = json.dumps({"code": 701})
			else:
				brain_response = brain.handleIncomingMessage(form["message"].value)
				content = json.dumps(brain_response)
			self.send_response(200)
		else:
			content = json.dumps({"code": 404})
			self.send_response(404)

		self.send_header('Content-Type','application/json')
		self.end_headers()
		self.wfile.write(bytes(content, "utf-8"))

class HTTPServerThread(threading.Thread):
	ps = None
	server_address = None
	httpd = None
	forceStop = False

	def __init__(self, ps):
		super(HTTPServerThread, self).__init__(name="HTTPServerThread")
		logging.info("HTTP Server Thread launching")
		self.ps = ps
		global http_ps
		http_ps = ps

	def run(self):
		try:
			prctl.set_name("HTTPServerThread")
		except NameError:
			pass
		self.serverStart()

	def serverStart(self):
		try:
			logging.info("HTTP server starting")
			self.server_address = ('', 9878)
			self.httpd = HTTPServer(self.server_address, HTTPServer_RequestHandler)
			self.run_forever()
		except:
			if Cfg.ENV.value <= Enviroment.Beta.value:
				logging.warning("HTTP server interrupted. Relaunching…")
				self.serverStart()
			else:
				logging.error("HTTP server interrupted. Not relaunching in dev env.")
				traceback.print_exc()

	def run_forever(self):
		isRunning = False

		while self.forceStop != True:
			if isRunning == False:
				logging.info("HTTP server started")
				logging.info("Now listening on http://127.0.0.1:9878")
				browser = True
				for arg in sys.argv:
					if str(arg) in ("-q", "--no-browser"):
						browser = False
				if browser:		
					webbrowser.open("http://127.0.0.1:9878")
				isRunning = True

				self.httpd.serve_forever()

