#!/usr/bin/env python3
import time
import os
import sys
from sys import platform
import datetime
import atexit
import threading
import traceback
import logging
if platform == "linux" or platform == "linux2":
	import prctl
from tm import ThreadManager
from config import Cfg, Enviroment
from classes import ProgramState

program_state = ProgramState()

def start():
	logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)-8s] %(threadName)-23s - %(message)s')
	logging.info("Application started")
	global program_state
	tm = ThreadManager(program_state)
	tm.begin()

def main():
	try:
		prctl.set_name("MainThread")
	except NameError:
		pass
	try:
		start()
	except KeyboardInterrupt:
		print("")
		logging.info("Termination signal from user")
		try:
			sys.exit(254)
		except SystemExit:
			os._exit(255)
	except:
		if Cfg.ENV.value <= Enviroment.Beta.value:
			logging.warning("Program interrupted. Relaunching…")
			main()
		else:
			logging.error("Program interrupted. Not relaunching in dev env.")
			traceback.print_exc()
		
@atexit.register
def onExit():
	if Cfg.ENV.value <= Enviroment.Beta.value:
		logging.warning("Program terminated. Relaunching…")
		main()
	else:
		logging.error("Program terminated. Not relaunching in dev env.")

if __name__ == '__main__':
	main()