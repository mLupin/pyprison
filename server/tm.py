import time
import os
import sys
import datetime
import threading
import logging
import ctypes
import ctypes.util
from httpserverthread import HTTPServerThread
from brain import BrainThread
from map import MapThread
from config import Cfg, Enviroment

class ThreadManager():
	program_state = None
	def __init__(self, ps):
		logging.info("ThreadManager initiated")
		self.program_state = ps

	def begin(self):
		logging.info("ThreadManager started")

		libpthread_path = ctypes.util.find_library("pthread")
		if libpthread_path:
			libpthread = ctypes.CDLL(libpthread_path)
			if hasattr(libpthread, "pthread_setname_np"):
				pthread_setname_np = libpthread.pthread_setname_np
				pthread_setname_np.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
				pthread_setname_np.restype = ctypes.c_int

				orig_setter = threading.Thread.__setattr__

				def attr_setter(self, name, value):
					orig_setter(self, name, value)
					if name == "name":
						ident = getattr(self, "ident", None)
						if ident:
							try:
								pthread_setname_np(ident, str(value))
							except:
								pass  
				threading.Thread.__setattr__ = attr_setter
				logging.info("Successfully overriden attr_setter method")

		while 1:
			for t in self.program_state.Threads:
				if not t.isAlive():
					self.program_state.Threads.remove(t)

			threads_to_load = Cfg.THREADS
			for t in self.program_state.Threads:
				if t.__class__.__name__ in threads_to_load:
					threads_to_load.remove(t.__class__.__name__)

			if threads_to_load:
				for t in threads_to_load:
					if t == "BrainThread":
						nt = BrainThread(self.program_state)
						self.program_state.brain = nt
						nt.start()
						self.program_state.Threads.append(nt)
					elif t == "HTTPServerThread":
						nt = HTTPServerThread(self.program_state)
						nt.start()
						self.program_state.Threads.append(nt)
					elif t == "MapThread":
						nt = MapThread(self.program_state)
						nt.start()
						self.program_state.Threads.append(nt)
			time.sleep(2)
