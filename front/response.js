function Response() {}

Response.stats = (function(data) {
	var stat = "";
	switch(data.stat) {
		case "A":
			stat = "hunger";
			break;
		case "B":
			stat = "hygiene";
			break;
		case "C":
			stat = "energy";
			break;
		case "D":
			stat = "entertainment";
			break;
		case "E":
			stat = "social";
			break;
	}
	var level = "";
	if(data.level > 75)
		level = "high";
	else if(data.level > 30)
		level = "medium";
	else
		level = "low";

	addMessage(2, getResponse(stat+"_"+level, {level: data.level}));
});

Response.basic = (function(data) {
	addMessage(2, getResponse(data.response));
});

Response.walk = (function(data) {
	addMessage(2, getResponse("walking", {direction: data.direction, count: data.count, s: __s(data.count), es: __es(data.count)}));
});

Response.cat = (function(data) {
	addMessage(2, data.fact);
});

Response.eat = (function(data) {
	addMessage(2, getResponse("after_eat"));
});

Response.bath = (function(data) {
	addMessage(2, getResponse("after_hygiene"));
});

Response.sleep = (function(data) {
	addMessage(2, getResponse("after_energy"));
});

Response.play = (function(data) {
	addMessage(2, getResponse("after_entertainment"));
});

Response.social = (function(data) {
	addMessage(2, getResponse("after_social"));
});
